November 13th, 2017

I have been to the UK and determined we would not return in the next five years. I try to get back into working hard on the novel. It's tough. I have to read over the drafts I have already done. I realise what I have taken on, but also, I realise that I am doing it. I am living this life now, and it is worth it.

I have a tricky week, working in little sprints, reorganising the living room, setting up English lessons and, perhaps (I forget if it was before or after the England trip), helping to make a huge sand pit at Woodstock's school.

I start working hard on getting rid of the last strongholds of yeast in my system. I use tea tree oil and some Cannisten wax, take baths and scrape at the stuff, even cutting off large chunks of fungal toenail with a paring knife.

For this, I get die off. What the hippies call a healing crisis. Meanwhile, I struggle to concentrate and planning is hell. While working on the first few drafts, I smoked a lot of dope. My plants have been growing now for months, but are only now beginning to flower. I start reading up on growing techniques and the effects of different strains of marijuana, and begin to have involved discussions on the interviews I have to talk about my novel about how marjuana helps the symptoms of what we might call my ADHD, my concentration problems. It helps my anxiety with my asperger's syndrome.

It seems unfair that I will have so little cannabis from the two plants I am growing, that it is difficult to buy the right kind of mellow hit from the street, that CBD oil and capsules are so expensive. I start to think about building a grow cabinet in our flat. I am obsessed - I have been more obsessed of late. I have had days depressed after taking probiotics, days where I have to sleep from fatigue; days where my head doesn't work.

Woodstock, meanwhile, sets up an evening on a Sunday where we will go to a friends' place to chat. She thinks they may have some marijuana.

We used to get marijuana from a friend with whom we would chat into he night, smoking thin little joints she would roll with the high-quality marijuana a friend would give to her. My latest phase of life began with a rugby-loving PE teacher with whom I would smoke dope in his flat on the Žižkov side of Riegrové sady and in a bar over that way.

That Sunday, I woke early as I had been. I got down to it a little, but, really, wasn't working on the novel but a creative commons journal called Marginálie I had been dreaming about for some time. To me, these were not separate projects. Though they both, individually, were huge, I had bundled them together early on. I would serialise Call Them Soldiers in Marginalie.

In recent days I had become irritated with a friend who had failed to get involved in a project of mine, failed even so much as to show interest enough to click a link to look into what it was about. Maybe this was fair of me and maybe it wasn't. It may have been related to the fact that I was autistic. Still, I had wanted to set something up with a few friends and since getting back, I was sure that I had no friends, no community, and that I could not do any of this.

One day, a Saturday, I was irritated. He had not written back to me. He was a friend of Woodstock, not me. I just made up the numbers. I then looked a little further into a book he had given me. This guy was bought. He had written to Milo [] at Breitbart speculating about the birth gender of a critic. He had written against Linux for years at a time. He was a tool.

Maybe this was true and maybe it wasn't. Maybe too, he had been disappointed at me first for not taking seriously his plans to set up a cafe.

So, I get up early on the Sunday, write a little, finish a book I am hoping to review for the first issue of Marginálie, make some notes, get myself tied up in knots a little, get anxious, feeling like I need to really make a start on the next draft of the novel, and then take a bath in installments, trying to sweat out some of that die off.

I am lost, between things, and leave the house an hour early to walk to the wrong tube station to meet Woodstock, having to turn back to sit and read for a while. Between routines, I am scarcely capable of functioning.

We take the metro and then the bus out. I have been unsettled for a couple of days. I have no friends. I see how alone I have been for so long. How few friends I have to have actual conversations, who is actually able to understand what I am trying to do.

I don't talk on metros. Don't talk on busses. I read. A comic this time, North Korea by Guy Delisle. From the library. I don't have so much cash at the moment, though it is true that I do buy books regularly, and bought a copy of American Splendour just a while ago, with Plastic Paddy of the Universe going up my priorities list.

Getting off the bus, I tell Woodstock not to expect that we will be on our own there. We get there and there is a BMW with Swiss plates outside the house. I begin to dread it. And yet, not really. In the event it is only us and this guy who they both know well. We sit and chat. He goes climbing. He grows marijuana. We chat about that for a while, I get my ideas straight about what it would involve. He works for Google. We get in and chat, spend a little time with the children. We play a game in which we try to guess which pictures our friends will like.

I sit afterwards and play the guitar a little while. We chat. I get out a book of photography and sit on a bed-like sofa. We don't have a sauna after all. I ask about another book, the Golden Bowl, which is up on the shelf. I have read about it, not read it.

And then it happens. We get to talking. Woodstock talks about how we can be in and out of phase. I process and then talk about what I have been thinking about. When I am least listening to them is when I have been most listening to them. They have four, five, six cycles to my one.

We talk about children and what we want from life. We talk about community and what it means. We talk about bitcoin and Gabča's husband, Dušan, who had loved all of the ideology behind it.

With the vaporisor, it is difficult to judge how much you are smoking. A cookie may kick in very slowly, a .

Through talking about marijuana, we get on to talking about why Gabča doesn't take it. Woodstock is surprised by how responsible she is. She really is a mother. She seems to play down how potentially dangerous marijuana could be. Gabča is breast feeding right now. I say no, alcohol breaks down quickly and predictably. Marijuana stays in the system for 30 days. I talk about foetal alcohol syndrome. Though I am sceptical about some of the stuff in the DSM IV and though I think mothers are blamed for everything, and though I have never specifically read up on foetal alcohol syndrome and "know" only what I heard from a self-serving residential head in the college I used to work at - the one I talked about with [], Woodstock's ex - I talk about what he said about foetal alcohol syndrome, how it can affect emotions and guilt etc. Woodstock says, wow, she can understand, but it is still incredible to her that somebody can think of all of these things. But I say, you haven't needed to. I mansplain about how, as soon as you step up, as soon as you have another life inside of you, you start thinking back to the choices your parents made, and forward to how this could affect your child thirty odd years on. You cannot but think about it, and suddenly you are tied into all of the things your family means. My family means more than what Woodstock sees now. What I see now. We came home thinking how boring it all was. And it is. There was so little conversation as we sat there talking about the price of life.

I am in my head in phases. Gabča talks about the phases of her life, and the things you don't see with bringing children into the world. You see the work. You don't see what else you get from it.

I start to think. Not long before, my father called. A friend of his was likely soon to pass away. We are the same age, he says. I am autistic, and don't take hints too well. He has recently inherited money from my uncle, his older brother, who passed away. He will be putting money into my account every month. I was in another world completely. I always have been. He was trying to wake me up to this to a certain extent.

I see now that it is my turn. I am nearly forty. The world has barely made an impact on me. I still don't feel like I could get married. I don't feel ready. I have always been marginal. I see now what my father achieved with all of his hard work. They got me to the point where I could begin to succeed. Now is the pay off when they begin to see what it all means. The family can't now die out. Woodstock's could.

As the women talk about families, I begin to think about what it means for me. I want to write. I want to create. I want to make a difference. But then what did my parents try to do. Gabča is making a difference by having her children who do electronics projects and play board games with the adults. I am not as stable as my father. I am not a grown up, it seems to me.

We talk about what community used to mean. Woodstock talks about how nobody was drinking at the film festival. We talk about the kid who came to our house to sit on the shitter Facebooking about us. He dumped his girlfriend as she was the maid of honour at a friends' wedding. He was thrown out. Men with cards in pubs. Music in pubs. Simply turning up and being asked to sing or perform until you can improve.

I begin to think about A Portrait of the Artist as a Young Man[ic Depressive]. About Debugging. It will be my major work. It goes back to Ireland and ought to go over what I inherited from my family.

My father grafted. He bought a set of encyclopedias. Bought us a computer. He wanted it to be an educational machine. It was. There was the BBC. There was BBC BASIC. It worked, even if it didn't take for years.

He was a grown up. I never have been. I have to get well. I am doing so now. With woodstock.

[] can talk like this sober.

[]'s husband is not quite like this. He lost what he had as a kid. He is no longer so much in love with the guy who set up Paralelni polis, and he is not a big reader. After a movie or such, they can have a conversation, but it doesn't typically go deeper than that.

I begin to think about 

YouTube party, 2005, in a book about Normalisation. I had come over to Pop Star and Facebook, to the beginnings of the end of Czech civil society.

We got back. I cook a little of the chicken. We go to bed, get up far too early. I read the Guardian on my phone, try to log in to Audible. I listen to an audiobook about philanthropists.